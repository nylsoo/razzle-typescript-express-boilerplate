import React from 'react';
import renderer from 'react-test-renderer';

import Home from '../../../scenes/Home';

it('renders Home scene correctly', () => {
  const tree = renderer.create(<Home />).toJSON();
  expect(tree).toMatchSnapshot();
});
